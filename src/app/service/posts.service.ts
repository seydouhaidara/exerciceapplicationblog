import { Injectable } from '@angular/core';
import { Post } from '../models/posts.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsSubject = new Subject<Post[]>();

  //L'array des posts
  listePoste = [
    new Post('Le post de seydou', 'Ce poste est le premier d\'une longue liste à venir.', 1, new Date()),
    new Post('Le post de Chantal', 'Je ne sais pas quoi mettre mais j\'essais quand même.', 0, new Date('2018-03-10')),
    new Post('Le poste de l\'inconnu', 'Je ne fais que lire les autres posts.', -1, new Date('2018-01-08')),
  ];

  constructor() { }

  emitPostsSubject() {
    this.postsSubject.next(this.listePoste.slice());
  }

  addPost(post: Post) {
    this.listePoste.push(post);
    this.emitPostsSubject();
  }

  changePostLoveIts(index: number, loveIts: number) {
    this.listePoste[index].loveIts = loveIts;
    this.emitPostsSubject();
  }

  deletePost(indexPostASupprimer: number) {
    this.listePoste.splice(indexPostASupprimer, 1);
    this.emitPostsSubject();
  }
}
