import { Component } from '@angular/core';
import { Post } from './models/posts.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //L'array des posts
  listePoste = [
    new Post('Le post de seydou', 'Ce poste est le premier d\'une longue liste à venir.', 1, new Date()),
    new Post('Le post de Chantal', 'Je ne sais pas quoi mettre mais j\'essais quand même.', 0, new Date('2018-03-10')),
    new Post('Le poste de l\'inconnu', 'Je ne fais que lire les autres posts.', -1, new Date('2018-01-08')),
  ];
}


