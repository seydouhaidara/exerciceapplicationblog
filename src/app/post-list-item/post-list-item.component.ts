import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../service/posts.service';
import { Subscription } from 'rxjs';
import { post } from 'selenium-webdriver/http';
import { Post } from '../models/posts.model';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  //Membres
  @Input() title: string;
  @Input() content: string;
  @Input() loveIts:number;
  @Input() created_at: Date;
  @Input() index: number;

  constructor(private postService: PostsService) { }

  ngOnInit() {
  }
  
  //Ajouter un like
   UpdateLoveIts(){
    this.loveIts++;
    this.onLoveItsChanged();
  }

  //Diminuer un like
  UpdateDontLoveIts(){
    this.loveIts--;
    this.onLoveItsChanged();
  }

  onLoveItsChanged(){
    this.postService.changePostLoveIts(this.index, this.loveIts);
  }

  onDeletePost(){
    this.postService.deletePost(this.index);
  }

}
