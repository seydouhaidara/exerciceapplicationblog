import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Post } from '../models/posts.model';
import { PostsService } from '../service/posts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

   //Membres
   listePoste: Post[];
   postSubscription: Subscription;
 
  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postSubscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => { 
        this.listePoste = posts
      }
    );
    this.postsService.emitPostsSubject();

  }

  ngOnDestroy(){
    this.postSubscription.unsubscribe();
  }

}
