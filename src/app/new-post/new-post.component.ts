import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostsService } from '../service/posts.service';
import { Router } from '@angular/router';
import { Post } from '../models/posts.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  newPostForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private postsService: PostsService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.newPostForm = this.formBuilder.group({
      titre: ['', Validators.required],
      content: ['', Validators.required]
    });

  }

  onSubmitForm(){
    //On récupère le contenu du formulaire
    const formValue = this.newPostForm.value;
    const titre = formValue['titre'];
    const content = formValue['content'];

    //On crée un nouveau post avec les éléments du formulaire et on l'ajoute à l'array
    const newPost = new Post(titre, content, 0, new Date());
    this.postsService.addPost(newPost);

    //On redirige vers la page de la liste des posts
    this.router.navigate(['/posts']);
  }

}
